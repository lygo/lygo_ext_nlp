package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_ext_nlp"
	"fmt"
	"testing"
)

func TestNlpTokenizer(t *testing.T) {
	text := "word longggggggggggg longggggggggggg and short, o q w e rrr "
	keywords := lygo_ext_nlp.GetKeywordsSorted(text, 3)
	fmt.Println("Keywords",keywords)

	km := lygo_ext_nlp.MapKeywords(text, 3)
	fmt.Println("Map:", km)

	score := lygo_ext_nlp.ScoreBest(text, []string{"longggggggg*", "cat*"})
	fmt.Println("ScoreBest:", score)
	score = lygo_ext_nlp.ScoreBest(text, []string{"w??*", "longggggggg*"})
	fmt.Println("ScoreBest:", score)

	score = lygo_ext_nlp.ScoreAnd(text, []string{"longggggggg*", "cat*"})
	fmt.Println("ScoreAll:", score)
	score = lygo_ext_nlp.ScoreAnd(text, []string{"w??*", "longggggggg*"})
	fmt.Println("ScoreAll:", score)

	score = lygo_ext_nlp.ScoreOr(text, []string{"longggggggg*", "cat*"})
	fmt.Println("ScoreAny:", score)
	score = lygo_ext_nlp.ScoreOr(text, []string{"w??*", "longggggggg*"})
	fmt.Println("ScoreAny:", score)
}

func TestLanguageDetection(t *testing.T) {
	lang := lygo_ext_nlp.GetLanguageCode("Ciao, questa è una frase in italiano. Non confonderla col catalano")
	fmt.Println(lang)
}

func TestEntitiesDetection(t *testing.T) {
	text, err := lygo_io.ReadTextFromFile("./text.txt")
	if nil!=err{
		t.Error(err)
		t.FailNow()
	}
	response := lygo_ext_nlp.GetEntitiesAll(text)
	fmt.Println("entities", response)
	lang := lygo_ext_nlp.GetLanguageCode(text)
	fmt.Println("Language",lang)
}
