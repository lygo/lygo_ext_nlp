package _test

import (
	"bitbucket.org/lygo/lygo_ext_nlp/lygo_nlpsumlexrank"
	"fmt"
	"io/ioutil"
	"testing"
)

func TestLexRankSummarize(t *testing.T) {
	intoSentences := 3
	textB, err := ioutil.ReadFile("./textlong.txt")
	if nil!=err{
		t.Error(err)
		t.FailNow()
	}

	text := string(textB)
	intoSentences = lygo_nlpsumlexrank.HighSummarySentences(text)
	summarizer := lygo_nlpsumlexrank.NewSummarizer()
	//summarizer.Algorithm = lygo_nlpsumlexrank.AlgCentrality
	result, _ := summarizer.Summarize(text, intoSentences)
	for _, row:=range result{
		fmt.Println(row)
	}

}
