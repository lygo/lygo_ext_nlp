package _test

import (
	"bitbucket.org/lygo/lygo_ext_nlp/lygo_nlpnum2word"
	"fmt"
	"testing"
)

func TestSimple(t *testing.T) {

	converter := lygo_num2word.NewNum2Word()

	exp := "uno"
	out := converter.Convert(1, "it")
	fmt.Println(exp, out)
	// assert.EqualValues(t, exp, out, "Expected %s, got %s", exp, out)

	exp = "mille cinquecento quarantadue"
	out = converter.Convert(1542, "it")
	fmt.Println(exp,out)
	// assert.EqualValues(t, exp, out, "Expected %s, got %s", exp, out)

	exp = "due mila cinquecento quarantadue"
	out = converter.Convert(2542, "it")
	fmt.Println(exp,out)
	// assert.EqualValues(t, exp, out, "Expected %s, got %s", exp, out)

	converter.Options.WordSeparator = ""
	exp = "duemilacinquecentoquarantadue"
	out = converter.Convert(2542, "it")
	fmt.Println(exp,out)
	// assert.EqualValues(t, exp, out, "Expected %s, got %s", exp, out)

}
