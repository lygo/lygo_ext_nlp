package _test

import (
	"bitbucket.org/lygo/lygo_ext_nlp/lygo_nlpsumlexrank"
	"fmt"
	"io/ioutil"
	"testing"
)

const (
	SAMPLE_FILE_PATH = "./text.txt"
	NUM_SENTENCES    = 3
)

var (
	benchSummarizer *lygo_nlpsumlexrank.Summarizer
	benchText       string
	resultText      []string
)

func init() {
	raw, err := ioutil.ReadFile(SAMPLE_FILE_PATH)
	if err != nil {
		panic(err)
	}
	benchText = string(raw)
}

func BenchmarkSummarizeCentralityHamming(b *testing.B) {
	var rtxt []string

	for n := 0; n < b.N; n++ {
		benchSummarizer = lygo_nlpsumlexrank.NewSummarizer()
		benchSummarizer.Algorithm = "centrality"
		benchSummarizer.Weighing = "hamming"
		rtxt, _ = benchSummarizer.Summarize(benchText, NUM_SENTENCES)
	}

	resultText = rtxt

	fmt.Println(rtxt)
}

func BenchmarkSummarizeCentralityJaccard(b *testing.B) {
	var rtxt []string

	for n := 0; n < b.N; n++ {
		benchSummarizer = lygo_nlpsumlexrank.NewSummarizer()
		benchSummarizer.Algorithm = "centrality"
		benchSummarizer.Weighing = "jaccard"
		rtxt, _ = benchSummarizer.Summarize(benchText, NUM_SENTENCES)
	}

	resultText = rtxt
}

func BenchmarkSummarizePagerankHamming(b *testing.B) {
	var rtxt []string

	for n := 0; n < b.N; n++ {
		benchSummarizer = lygo_nlpsumlexrank.NewSummarizer()
		benchSummarizer.Algorithm = "pagerank"
		benchSummarizer.Weighing = "hamming"
		rtxt, _ = benchSummarizer.Summarize(benchText, NUM_SENTENCES)
	}

	resultText = rtxt
}

func BenchmarkSummarizePagerankJaccard(b *testing.B) {
	var rtxt []string

	for n := 0; n < b.N; n++ {
		benchSummarizer = lygo_nlpsumlexrank.NewSummarizer()
		benchSummarizer.Algorithm = "pagerank"
		benchSummarizer.Weighing = "jaccard"
		rtxt, _ = benchSummarizer.Summarize(benchText, NUM_SENTENCES)
	}

	resultText = rtxt
}
