package lygo_ext_nlp

import (
	"bitbucket.org/lygo/lygo_commons/lygo"
	"bitbucket.org/lygo/lygo_commons/lygo_regex"
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"bitbucket.org/lygo/lygo_ext_nlp/lygo_nlpdetect"
	"bitbucket.org/lygo/lygo_ext_nlp/lygo_nlpnum2word"
	"bitbucket.org/lygo/lygo_ext_nlp/lygo_nlpsumlexrank"
	"fmt"
	"regexp"
	"strings"
)

//----------------------------------------------------------------------------------------------------------------------
//	d e t e c t    l a n g u a g e
//----------------------------------------------------------------------------------------------------------------------

const (
	entityBtc            = "btc"
	entityStreet         = "street"
	entityIP             = "ip"
	entityIPv4           = "ipv4"
	entityIPv6           = "ipv6"
	entityEmail          = "email"
	entityNumber         = "number"
	entityDate           = "date"
	entityTime           = "time"
	entityPhone          = "phone"
	entityLink           = "link"
	entityPort           = "port"
	entityPrice          = "price"
	entityHexColor       = "hex-color"
	entityCreditCard     = "credit-card"
	entityVisaCreditCard = "visa-credit-card"
	entityMCCreditCard   = "mastercard-credit-card"
	entityZipCode        = "zip-code"
	entityMACAddress     = "mac-address"
	entityIBAN           = "iban"
)

var (
	EntitiesAll = []string{
		entityBtc,
		entityStreet,
		entityIP,
		entityIPv4,
		entityIPv6,
		entityEmail,
		entityNumber,
		entityDate,
		entityTime,
		entityPhone,
		entityLink,
		entityPort,
		entityPrice,
		entityHexColor,
		entityCreditCard,
		entityVisaCreditCard,
		entityMCCreditCard,
		entityZipCode,
		entityMACAddress,
		entityIBAN,
	}

	EntitiesBasic = []string{
		entityEmail,
		entityNumber,
		entityNumber,
		entityDate,
		entityTime,
		entityPhone,
		entityLink,
		entityZipCode,
		entityStreet,
	}

	EntitiesBank = []string{
		entityCreditCard,
		entityVisaCreditCard,
		entityMCCreditCard,
		entityIBAN,
	}

	EntitiesNetwork = []string{
		entityMACAddress,
		entityIP,
		entityIPv6,
		entityIPv4,
		entityPort,
		entityBtc,
	}
)

//----------------------------------------------------------------------------------------------------------------------
//	d e t e c t    l a n g u a g e
//----------------------------------------------------------------------------------------------------------------------

func GetLanguageCode(text string) string {
	return lygo_nlpdetect.DetectOne(text).Code
}

//----------------------------------------------------------------------------------------------------------------------
//	n u m 2 w o r d
//----------------------------------------------------------------------------------------------------------------------

func Num2Word(num int, lang ...string) string {
	converter := lygo_num2word.NewNum2Word()
	if len(lang) == 1 {
		return converter.Convert(num, lang[0])
	}
	return converter.ConvertDefault(num)
}

//----------------------------------------------------------------------------------------------------------------------
//	k e y w o r d s
//----------------------------------------------------------------------------------------------------------------------

func GetKeywords(text string, minLen int) []string {
	exp := regexp.MustCompile(fmt.Sprintf("\\b\\w{%v,100}\\b", minLen))
	return exp.FindAllString(text, -1)
}

func GetKeywordsSorted(text string, minLen int) []string {
	keywords := GetKeywords(text, minLen)
	lygo.Arrays.Sort(keywords)
	return keywords
}

// GetKeywordsProgression generate an array of keywords with also partial words like ["word", "wor", "wo"].
// This is useful searching for partial words
func GetKeywordsProgression(text string, minLen int) []string {
	limit := minLen - 1
	response := make([]string, 0)
	if len(text) > 0 {
		tokens := lygo_strings.Split(text, " ,.!?")
		for _, token := range tokens {
			if len(token) > limit {
				if len(token) > limit+2 {
					for i := 0; i < len(token)-2; i++ {
						response = append(response, token[:len(token)-i])
					}
				} else {
					response = append(response, token)
				}
			}
		}
	}
	return response
}

func MapKeywords(text string, minLen int) map[string]int {
	response := map[string]int{}
	keywords := GetKeywords(text, minLen)
	for _, k := range keywords {
		if _, b := response[k]; !b {
			response[k] = 0
		}
		response[k] = response[k] + 1
	}
	return response
}

//----------------------------------------------------------------------------------------------------------------------
//	s c o r e
//----------------------------------------------------------------------------------------------------------------------

// ScoreBest Calculate a matching score between a phrase and a check test using expressions.
// ALL expressions are evaluated.
// Failed expressions  do not add negative score to result.
// Return best score above all
// @param [string] phrase. "hello humanity!! I'm Mario rossi"
// @param [string] expressions. All expressions to match. ["hel??0 h*", "I* * ros*"]
func ScoreBest(text string, expressions []string) float32 {
	return lygo_regex.WildcardScoreBest(normalize(text), expressions)
}

// ScoreOr Calculate a matching score between a phrase and a check test using expressions.
// ALL expressions are evaluated.
// Failed expressions  do not add negative score to result
// @param [string] phrase. "hello humanity!! I'm Mario rossi"
// @param [string] expressions. All expressions to match. ["hel??0 h*", "I* * ros*"]
func ScoreOr(text string, expressions []string) float32 {
	return lygo_regex.WildcardScoreAny(normalize(text), expressions)
}

// ScoreAnd Calculate a matching score between a phrase and a check test using expressions.
// ALL expressions are evaluated.
// Failed expressions  add negative score to result
// @param [string] phrase. "hello humanity!! I'm Mario rossi"
// @param [string] expressions. All expressions to match. ["hel??0 h*", "I* * ros*"]
func ScoreAnd(text string, expressions []string) float32 {
	return lygo_regex.WildcardScoreAll(normalize(text), expressions)
}

//----------------------------------------------------------------------------------------------------------------------
//	e n t i t i e s
//----------------------------------------------------------------------------------------------------------------------

func MatchEntitiesAll(text string) map[string][]string {
	return MatchEntities(text, EntitiesAll)
}

func MatchEntities(text string, list []string) map[string][]string {
	response := make(map[string][]string)
	for _, name := range list {
		data := entityByName(name, text)
		response[name] = data
	}
	return response
}

// GetEntitiesAll return only existing entities
func GetEntitiesAll(text string) map[string][]string {
	return GetEntities(text, EntitiesAll)
}

func GetEntities(text string, list []string) map[string][]string {
	response := make(map[string][]string)
	entities := MatchEntities(text, list)
	for k, v := range entities {
		if len(v) > 0 {
			response[k] = v
		}
	}
	return response
}

//----------------------------------------------------------------------------------------------------------------------
//	s u m m a r i z e
//----------------------------------------------------------------------------------------------------------------------

// SummarizeLexRankHigh very short text. Response is about 15% of original text
func SummarizeLexRankHigh(text string) []string {
	return SummarizeLexRank(text, lygo_nlpsumlexrank.HighSummarySentences(text))
}

// SummarizeLexRankMedium response is about 25% of original text
func SummarizeLexRankMedium(text string) []string {
	return SummarizeLexRank(text, lygo_nlpsumlexrank.MediumSummarySentences(text))
}

// SummarizeLexRankLow response is about 40% of original text
func SummarizeLexRankLow(text string) []string {
	return SummarizeLexRank(text, lygo_nlpsumlexrank.LowSummarySentences(text))
}

func SummarizeLexRank(text string, intoSentences int) []string {
	summarizer := lygo_nlpsumlexrank.NewSummarizer()
	result, _ := summarizer.Summarize(text, intoSentences)

	return result
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func normalize(text string) string {
	tokens := GetKeywords(text, 3)
	return strings.Join(tokens, " ")
}

func entityByName(name, text string) []string {
	switch name {
	case entityBtc:
		return lygo_regex.BtcAddresses(text)
	case entityStreet:
		return lygo_regex.StreetAddresses(text)
	case entityIP:
		return lygo_regex.IPs(text)
	case entityIPv4:
		return lygo_regex.IPv4s(text)
	case entityIPv6:
		return lygo_regex.IPv6s(text)
	case entityEmail:
		return lygo_regex.Emails(text)
	case entityNumber:
		return lygo_regex.Numbers(text)
	case entityDate:
		return lygo_regex.Date(text)
	case entityTime:
		return lygo_regex.Time(text)
	case entityPhone:
		response := lygo_regex.Phones(text)
		response = lygo.Arrays.AppendUnique(response, lygo_regex.PhonesWithExts(text)).([]string)
		return response
	case entityLink:
		return lygo_regex.Urls(text)
	case entityPort:
		return lygo_regex.NotKnownPorts(text)
	case entityPrice:
		return lygo_regex.Prices(text)
	case entityHexColor:
		return lygo_regex.HexColors(text)
	case entityCreditCard:
		return lygo_regex.CreditCards(text)
	case entityVisaCreditCard:
		return lygo_regex.VISACreditCards(text)
	case entityMCCreditCard:
		return lygo_regex.MCCreditCards(text)
	case entityZipCode:
		return lygo_regex.ZipCodes(text)
	case entityMACAddress:
		return lygo_regex.MACAddresses(text)
	case entityIBAN:
		return lygo_regex.IBANs(text)
	}
	return make([]string, 0)
}
