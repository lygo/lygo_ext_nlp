# nlp Binary Executable #

This directory contains executables to test nlp module.

Supported Platforms:

 - [Linux](./linux)
 - [Windows](./windows)
 - [Mac](./mac)
 - [Rasberry](./linux_raspberry_3)

## Supported Commands ##

### summarize ###

Arguments:

 - file: (Required) Filename to read and summarize.
 - sentences: (Optional) Number of sentences. Default is 3 sentences.

Sample:
```
nlp summarize -file=./myfile.txt -sentences=4
```

On Windows use: nlp.exe

Expected Output: 

Command generates an output file "./summary-myfile.txt" containing a text summary 
