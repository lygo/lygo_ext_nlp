package main

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"bitbucket.org/lygo/lygo_ext_nlp"
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
)

// ---------------------------------------------------------------------------------------------------------------------
//	m a i n
// ---------------------------------------------------------------------------------------------------------------------

func main() {
	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			message := lygo_strings.Format("[panic] APPLICATION %s ERROR: %s", "NLP", r)
			fmt.Println(message)
		}
	}()

	//-- command flags --//
	// summarize
	cmdSummarize := flag.NewFlagSet("summarize", flag.ExitOnError)
	cmdSummarizeFile := cmdSummarize.String("file", "", "Set file to summarize")
	cmdSummarizeSentences := cmdSummarize.Int("sentences", 3, "Set number of sentences to summarize")

	// parse
	if len(os.Args) > 1 {
		cmd := os.Args[1]
		switch cmd {
		case "summarize":
			// BUILD
			_ = cmdSummarize.Parse(os.Args[2:])
			// run command
			summarize(*cmdSummarizeFile, *cmdSummarizeSentences)
		default:
			os.Stdout.Write([]byte("Command not implemented.\n"))
			// fmt.Println("Command not implemented.")
		}
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func summarize(file string, intoSentences int) {
	file = lygo_paths.Absolute(file)
	textB, err := ioutil.ReadFile(file)
	if nil != err {
		panic(err)
	}
	var buf bytes.Buffer
	response := lygo_ext_nlp.SummarizeLexRank(string(textB), intoSentences)
	for _, row := range response {
		buf.WriteString(row)
		buf.WriteString("\n")
	}
	out := lygo_paths.ChangeFileNameWithPrefix(file, "summary-")
	_, err = lygo_io.WriteBytesToFile(buf.Bytes(), out)
	if nil != err {
		panic(err)
	}

	_, _ = os.Stdout.Write([]byte(fmt.Sprintf("Response written in file: '%s'\n", out)))
}

func publish(file, dir string) {

}

func stub(file, target string) {

}
