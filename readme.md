# LyGo NLP #

![](./icon.png)

Simple general purpose NLP implementation.

## SAMPLE BINARY ##

[Here](./_build) is an executable binary version to test some of NLP module functionalities.

This is a work in progress.

## Dependencies

```
go get -u bitbucket.org/lygo/lygo_ext_scripting
```

## How to Use

To use just call:

```
go get -u bitbucket.org/lygo/lygo_ext_nlp@v0.1.11
```
### Versioning

Sources are versioned using git tags:

```
git tag v0.1.11
git push origin v0.1.11
```

 