module bitbucket.org/lygo/lygo_ext_nlp

go 1.16

require (
	bitbucket.org/lygo/lygo_commons v0.1.120
	bitbucket.org/lygo/lygo_ext_scripting v0.1.95
	github.com/arangodb/go-driver v0.0.0-20210427052136-0817df9ff66e // indirect
	github.com/dop251/goja v0.0.0-20210406175830-1b11a6af686d
	github.com/klauspost/compress v1.12.2 // indirect
	golang.org/x/sys v0.0.0-20210426230700-d19ff857e887 // indirect
)
