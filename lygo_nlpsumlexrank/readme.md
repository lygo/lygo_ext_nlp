## LexRank Algorithm ##

Derived from original project: https://github.com/JesusIslam/tldr

Some code adapted from:
https://github.com/NaturalNode/natural/blob/master/lib/natural/distance/jaro-winkler_distance.js


### What?

This module summarize a text automatically
using [lexrank](http://www.cs.cmu.edu/afs/cs/project/jair/pub/volume22/erkan04a-html/erkan04a.html) algorithm.

### How?

There are two main steps in lexrank, weighing, and ranking. This module have two weighing and two ranking algorithm
included, they are Jaccard coeficient and Hamming distance, then PageRank and centrality, respectively. The default
settings use Hamming distance and pagerank.
